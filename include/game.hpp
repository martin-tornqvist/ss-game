// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef GAME_HPP
#define GAME_HPP

#include "ecs.hpp"
#include "pos.hpp"
#include "state.hpp"

namespace game
{
struct World : ecs::ECS
{
        ecs::ECS ecs {};
};

struct PosComp
{
        float x {0.0F};
        float y {0.0F};
};

struct VelocityComp
{
        float dx {0.0F};
        float dy {0.0F};
};

class PlayerInputSys : public ecs::System
{
public:
        void update() override;
};

class MoveSys : public ecs::System
{
public:
        void update() override;
};

class DrawSys : public ecs::System
{
public:
        void update() override;
};

class GameState : public State
{
        void on_start() override;

        void update() override;

        void draw() override;
};

}  // namespace game

#endif  // GAME_HPP
