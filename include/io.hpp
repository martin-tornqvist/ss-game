// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef IO_HPP
#define IO_HPP

#include <string>

#include "SDL_stdinc.h"
#include "pos.hpp"

struct InputData
{
        int key_held {};
        int key_pressed {-1};
        int key_released {-1};
};

namespace io
{
void init();
void cleanup();

void update_screen();

void clear_screen();

bool is_key_held(int key);

InputData read_input_events();

void clear_events();

void flush_input();

// TODO: Placeholder
void draw_texture(Pos p);

}  // namespace io

#endif  // IO_HPP
