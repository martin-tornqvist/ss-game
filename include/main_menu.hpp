// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef MAIN_MENU_HPP
#define MAIN_MENU_HPP

#include "state.hpp"

class MainMenuState : public State
{
        void on_start() override;

        void update() override;

        void draw() override;
};

#endif  // MAIN_MENU_HPP
