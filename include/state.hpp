// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef STATE_HPP
#define STATE_HPP

#include <memory>
#include <string>
#include <vector>

class State
{
public:
        virtual ~State() = default;

        // Executed immediately when the state is pushed.
        virtual void on_pushed() {}

        // Executed the first time that the state becomes the current state.
        // Sometimes multiple states may be pushed in a sequence, and one of the
        // later states may want to perform actions only when it actually
        // becomes the current state.
        virtual void on_start() {}

        // Executed immediately when the state is popped.
        // This should only be used for cleanup, do not push or pop other states
        // from this call (this is not supported).
        virtual void on_popped() {}

        // All states above have been popped
        virtual void on_resume() {}

        // Another state is pushed on top
        virtual void on_paused() {}

        virtual void draw() {}

        // If true, this state is drawn overlayed on the state(s) below. This
        // can be used for example to draw a marker state on top of the map.
        virtual bool draw_overlayed() const
        {
                return false;
        }

        virtual void on_window_resized() {}

        // Read input, process game logic etc.
        virtual void update() {}

        bool has_started() const
        {
                return m_has_started;
        }

        void set_started()
        {
                m_has_started = true;
        }

        // virtual StateId id() const = 0;

private:
        bool m_has_started {false};
};

namespace states
{
void init();

void cleanup();

void run();

void run_until_state_done(std::unique_ptr<State> state);

void start();

void draw();

void update();

void push(std::unique_ptr<State> state);

void pop();

void pop_all();

bool is_empty();

bool is_current_state(const State* state);

bool contains_state(const State* state);

}  // namespace states

#endif  // STATE_HPP
