// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

namespace init
{
void cleanup_all();

void init_io();
void cleanup_io();

void init_session();
void cleanup_session();

}  // namespace init
