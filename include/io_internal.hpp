// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef IO_INTERNAL_HPP
#define IO_INTERNAL_HPP

#include "io.hpp"

#include "xml.hpp"

struct SDL_Window;
struct SDL_Renderer;
struct SDL_Texture;
struct SDL_Surface;

namespace io
{
class TextBlock;

extern SDL_Window* g_sdl_window;
extern SDL_Renderer* g_sdl_renderer;

// TODO: Placeholder
extern SDL_Texture* g_dude_texture;

void init_window();

void init_input();

SDL_Texture* load_texture(const std::string& path);

}  // namespace io

#endif  // IO_INTERNAL_HPP
