// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef POS_HPP
#define POS_HPP

struct Pos
{
public:
        Pos() = default;

        Pos(const int x_val, const int y_val) :
                x(x_val),
                y(y_val) {}

        Pos(const Pos& p) = default;

        Pos& operator=(const Pos& p) = default;

        Pos& operator+=(const Pos& p)
        {
                x += p.x;
                y += p.y;

                return *this;
        }

        Pos& operator-=(const Pos& p)
        {
                x -= p.x;
                y -= p.y;

                return *this;
        }

        Pos& operator++()
        {
                ++x;
                ++y;

                return *this;
        }

        Pos& operator--()
        {
                --x;
                --y;

                return *this;
        }

        Pos operator+(const Pos& p) const
        {
                return Pos(x + p.x, y + p.y);
        }

        Pos operator+(const int v) const
        {
                return Pos(x + v, y + v);
        }

        Pos operator-(const Pos& p) const
        {
                return Pos(x - p.x, y - p.y);
        }

        Pos operator-(const int v) const
        {
                return Pos(x - v, y - v);
        }

        Pos with_offsets(const int x_offset, const int y_offset) const
        {
                return Pos(x + x_offset, y + y_offset);
        }

        Pos with_offsets(const Pos& offsets) const
        {
                return Pos(x + offsets.x, y + offsets.y);
        }

        Pos with_x_offset(const int offset) const
        {
                return Pos(x + offset, y);
        }

        Pos with_y_offset(const int offset) const
        {
                return Pos(x, y + offset);
        }

        Pos scaled_up(const Pos& p) const
        {
                return Pos(x * p.x, y * p.y);
        }

        Pos scaled_up(const int x_factor, const int y_factor) const
        {
                return Pos(x * x_factor, y * y_factor);
        }

        Pos scaled_up(const int v) const
        {
                return Pos(x * v, y * v);
        }

        Pos scaled_down(const int x_denom, const int y_denom) const
        {
                return Pos(x / x_denom, y / y_denom);
        }

        Pos scaled_down(const int v) const
        {
                return Pos(x / v, y / v);
        }

        Pos scaled_down(const Pos& denoms) const
        {
                return Pos(x / denoms.x, y / denoms.y);
        }

        bool operator==(const Pos& p) const
        {
                return (x == p.x) && (y == p.y);
        }

        bool operator!=(const Pos& p) const
        {
                return (x != p.x) || (y != p.y);
        }

        bool operator!=(const int v) const
        {
                return (x != v) || (y != v);
        }

        Pos signs() const
        {
                int x_sign = 0;
                int y_sign = 0;

                if (x == 0)
                {
                        x_sign = 0;
                }
                else if (x > 0)
                {
                        x_sign = 1;
                }
                else
                {
                        x_sign = -1;
                }

                if (y == 0)
                {
                        y_sign = 0;
                }
                else if (y > 0)
                {
                        y_sign = 1;
                }
                else
                {
                        y_sign = -1;
                }

                return {x_sign, y_sign};
        }

        void set(const int new_x, const int new_y)
        {
                x = new_x;
                y = new_y;
        }

        void set(const Pos& p)
        {
                x = p.x;
                y = p.y;
        }

        void swap(Pos& p)
        {
                Pos tmp(p);

                p = *this;

                set(tmp);
        }

        bool is_adjacent(const Pos& p) const
        {
                // Do not count the same position as adjacent
                if (p == *this)
                {
                        return false;
                }

                const auto d = *this - p;

                const bool x_adj = (d.x >= -1) && (d.x <= 1);
                const bool y_adj = (d.y >= -1) && (d.y <= 1);

                return x_adj && y_adj;
        }

        int x {0};
        int y {0};
};

#endif  // POS_HPP
