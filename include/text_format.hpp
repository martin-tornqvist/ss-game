// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#ifndef TEXT_FORMAT_HPP
#define TEXT_FORMAT_HPP

#include <string>
#include <vector>

namespace text_format
{
// Reads a line of space separated words, and splits them into several lines
// with the given maximum width. If any single word in the "line" parameter is
// longer than the maximum width, this word will NOT be split (the entire word
// is simply added to the output vector, breaking the maximum width).
std::vector<std::string> split_width(std::string str, size_t max_w);

std::vector<std::string> split_delim(const std::string& str, char delim);

std::string first_to_lower(const std::string& str);
std::string first_to_upper(const std::string& str);
std::string to_upper(const std::string& str);

}  // namespace text_format

#endif  // TEXT_FORMAT_HPP
