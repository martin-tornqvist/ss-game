// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

// Credit
//
// The code in this file is mostly adapted from the following guide:
//
// Austin Morlan
// https://austinmorlan.com/posts/entity_component_system/

#include <algorithm>
#include <array>
#include <bitset>
#include <cstddef>
#include <cstdint>
#include <queue>
#include <set>
#include <unordered_map>

#include "debug.hpp"

namespace ecs
{
using EntityId = uint32_t;
const size_t max_entities = 5000;

using ComponentType = std::uint8_t;
const size_t max_components = 32;

using Signature = std::bitset<max_components>;

class EntityMgr
{
public:
        EntityMgr();

        EntityId create_entity();

        void destroy_entity(EntityId entity);

        void set_signature(EntityId entity, Signature signature);

        Signature get_signature(EntityId entity);

private:
        std::queue<EntityId> m_available_entity_ids {};

        std::array<Signature, max_entities> m_signatures {};

        uint32_t m_living_entity_count {};
};

class IComponentArray
{
public:
        virtual ~IComponentArray() = default;

        virtual void entity_destroyed(EntityId entity) = 0;
};

template <typename T>
class ComponentArray : public IComponentArray
{
public:
        void insert_data(const EntityId entity, T component)
        {
                ASSERT(
                        m_entity_to_idx_map.find(entity) ==
                        std::end(m_entity_to_idx_map));

                size_t new_idx = m_size;

                m_entity_to_idx_map[entity] = new_idx;

                m_idx_to_entity_map[new_idx] = entity;

                m_component_array[new_idx] = component;

                ++m_size;
        }

        void remove_data(const EntityId entity)
        {
                ASSERT(
                        m_entity_to_idx_map.find(entity) !=
                        std::end(m_entity_to_idx_map));

                auto idx_of_removed_entity = m_entity_to_idx_map[entity];

                size_t idx_of_last_element = m_size - 1;

                m_component_array[idx_of_removed_entity] =
                        m_component_array[idx_of_last_element];

                const auto entity_of_last_element =
                        m_idx_to_entity_map[idx_of_last_element];

                m_entity_to_idx_map[entity_of_last_element] =
                        idx_of_removed_entity;

                m_idx_to_entity_map[idx_of_removed_entity] =
                        entity_of_last_element;

                m_entity_to_idx_map.erase(entity);

                m_idx_to_entity_map.erase(idx_of_last_element);

                --m_size;
        }

        T& get_data(const EntityId entity)
        {
                ASSERT(
                        m_entity_to_idx_map.find(entity) !=
                        std::end(m_entity_to_idx_map));

                const auto idx = m_entity_to_idx_map[entity];

                return m_component_array[idx];
        }

        void entity_destroyed(const EntityId entity) override
        {
                const bool exists =
                        m_entity_to_idx_map.find(entity) !=
                        std::end(m_entity_to_idx_map);

                if (exists)
                {
                        remove_data(entity);
                }
        }

private:
        // Packed array of components (of generic type T), set to a specified
        // maximum amount, matching the maximum number of entities allowed to
        // exist simultaneously, so that each entity has a unique spot.
        std::array<T, max_entities> m_component_array {};

        std::unordered_map<EntityId, size_t> m_entity_to_idx_map {};

        std::unordered_map<size_t, EntityId> m_idx_to_entity_map {};

        // Total size of valid entries in the array.
        size_t m_size {0};
};

class ComponentMgr
{
public:
        template <typename T>
        void register_component()
        {
                const char* type_name = typeid(T).name();

                ASSERT(
                        m_component_types.find(type_name) ==
                        std::end(m_component_types));

                m_component_types.insert(
                        {type_name, m_next_component_type});

                m_component_arrays.insert(
                        {type_name, std::make_shared<ComponentArray<T>>()});

                ++m_next_component_type;
        }

        template <typename T>
        ComponentType get_component_type()
        {
                const char* type_name = typeid(T).name();

                ASSERT(
                        m_component_types.find(type_name) !=
                        std::end(m_component_types));

                return m_component_types[type_name];
        }

        template <typename T>
        void add_component(const EntityId entity, T component)
        {
                get_component_array<T>()->insert_data(entity, component);
        }

        template <typename T>
        void remove_component(const EntityId entity)
        {
                get_component_array<T>()->remove_data(entity);
        }

        template <typename T>
        T& get_component(const EntityId entity)
        {
                return get_component_array<T>()->get_data(entity);
        }

        void entity_destroyed(const EntityId entity)
        {
                for (auto const& pair : m_component_arrays)
                {
                        auto const& component = pair.second;

                        component->entity_destroyed(entity);
                }
        }

private:
        template <typename T>
        std::shared_ptr<ComponentArray<T>> get_component_array()
        {
                const char* type_name = typeid(T).name();

                ASSERT(
                        m_component_types.find(type_name) !=
                        std::end(m_component_types));

                return (
                        std::static_pointer_cast<ComponentArray<T>>(
                                m_component_arrays[type_name]));
        }

        std::unordered_map<const char*, ComponentType> m_component_types {};

        std::unordered_map<const char*, std::shared_ptr<IComponentArray>>
                m_component_arrays {};

        ComponentType m_next_component_type {};
};

class System
{
public:
        virtual void update() = 0;

        std::set<EntityId> m_entities;
};

class SystemMgr
{
public:
        template <typename T>
        std::shared_ptr<T> register_system()
        {
                const char* type_name = typeid(T).name();

                assert(m_systems.find(type_name) == std::end(m_systems));

                auto system = std::make_shared<T>();

                m_systems.insert({type_name, system});

                return system;
        }

        template <typename T>
        void set_signature(const Signature signature)
        {
                const char* type_name = typeid(T).name();

                assert(m_systems.find(type_name) != std::end(m_systems));

                // Set the signature for this system
                m_signatures.insert({type_name, signature});
        }

        void entity_destroyed(const EntityId entity)
        {
                for (auto const& pair : m_systems)
                {
                        auto const& system = pair.second;

                        system->m_entities.erase(entity);
                }
        }

        void entity_signature_changed(
                const EntityId entity,
                const Signature entity_signature)
        {
                // Notify each system that an entity's signature changed
                for (auto const& pair : m_systems)
                {
                        auto const& type = pair.first;
                        auto const& system = pair.second;
                        auto const& system_signature = m_signatures[type];

                        if ((entity_signature & system_signature) ==
                            system_signature)
                        {
                                // Entity signature matches system signature -
                                // insert into set.
                                system->m_entities.insert(entity);
                        }
                        else
                        {
                                // Entity signature does not match system
                                // signature - erase from set.
                                system->m_entities.erase(entity);
                        }
                }
        }

private:
        // Map from system type string pointer to a signature
        std::unordered_map<const char*, Signature> m_signatures {};

        // Map from system type string pointer to a system pointer
        std::unordered_map<const char*, std::shared_ptr<System>> m_systems {};
};

class ECS
{
public:
        void init()
        {
                m_component_mgr = std::make_unique<ComponentMgr>();
                m_entity_mgr = std::make_unique<EntityMgr>();
                m_system_mgr = std::make_unique<SystemMgr>();
        }

        // System methods
        template <typename T>
        std::shared_ptr<T> register_system()
        {
                return m_system_mgr->register_system<T>();
        }

        template <typename T>
        void set_system_signature(Signature signature)
        {
                m_system_mgr->set_signature<T>(signature);
        }

        template <typename T>
        void register_component()
        {
                m_component_mgr->register_component<T>();
        }

        template <typename T>
        void add_component(const EntityId entity, T component)
        {
                m_component_mgr->add_component<T>(entity, component);

                auto signature = m_entity_mgr->get_signature(entity);

                signature.set(m_component_mgr->get_component_type<T>(), true);

                m_entity_mgr->set_signature(entity, signature);

                m_system_mgr->entity_signature_changed(entity, signature);
        }

        template <typename T>
        void remove_component(const EntityId entity)
        {
                m_component_mgr->remove_component<T>(entity);

                auto signature = m_entity_mgr->get_signature(entity);

                signature.set(m_component_mgr->get_component_type<T>(), false);

                m_entity_mgr->set_signature(entity, signature);

                m_system_mgr->entity_signature_changed(entity, signature);
        }

        template <typename T>
        T& get_component(const EntityId entity)
        {
                return m_component_mgr->get_component<T>(entity);
        }

        template <typename T>
        ComponentType get_component_type()
        {
                return m_component_mgr->get_component_type<T>();
        }

        EntityId create_entity()
        {
                return m_entity_mgr->create_entity();
        }

        void destroy_entity(const EntityId entity)
        {
                m_entity_mgr->destroy_entity(entity);

                m_component_mgr->entity_destroyed(entity);

                m_system_mgr->entity_destroyed(entity);
        }

private:
        std::unique_ptr<ComponentMgr> m_component_mgr;
        std::unique_ptr<EntityMgr> m_entity_mgr;
        std::unique_ptr<SystemMgr> m_system_mgr;
};

}  // namespace ecs
