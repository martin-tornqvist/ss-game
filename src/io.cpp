// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "io.hpp"

#include "SDL.h"
#include "SDL_error.h"
#include "SDL_events.h"
#include "SDL_filesystem.h"
#include "SDL_image.h"
#include "SDL_mixer.h"
#include "SDL_pixels.h"
#include "SDL_rect.h"
#include "SDL_render.h"
#include "SDL_stdinc.h"
#include "SDL_surface.h"
#include "SDL_timer.h"
#include "SDL_video.h"
#include "debug.hpp"
#include "io_internal.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static SDL_Renderer* create_renderer()
{
        TRACE_FN_BEGIN;

        auto* const renderer =
                SDL_CreateRenderer(
                        io::g_sdl_window,
                        -1,
                        SDL_RENDERER_SOFTWARE);

        if (!renderer)
        {
                ERROR_FMT("Failed to create SDL renderer: {}", SDL_GetError());

                PANIC;
        }

        TRACE_FN_END;

        return renderer;
}

static void cleanup_sdl()
{
        if (!SDL_WasInit(SDL_INIT_EVERYTHING))
        {
                return;
        }

        IMG_Quit();

        // Mix_AllocateChannels(0);

        // Mix_CloseAudio();

        SDL_Quit();
}

static void init_sdl()
{
        TRACE_FN_BEGIN;

        cleanup_sdl();

        const uint32_t sdl_init_flags =
                SDL_INIT_VIDEO |
                SDL_INIT_AUDIO |
                SDL_INIT_EVENTS;

        if (SDL_Init(sdl_init_flags) == -1)
        {
                ERROR_FMT("Failed to init SDL: {}", SDL_GetError());

                PANIC;
        }

        const uint32_t sdl_img_flags = IMG_INIT_PNG;

        if (IMG_Init(sdl_img_flags) == -1)
        {
                ERROR_FMT("Failed to init SDL_image: {}", SDL_GetError());

                PANIC;
        }

        // const int audio_freq = 44100;
        // const Uint16 audio_format = MIX_DEFAULT_FORMAT;
        // const int audio_channels = 2;
        // const int audio_buffers = 1024;

        // const int result =
        //         Mix_OpenAudio(
        //                 audio_freq,
        //                 audio_format,
        //                 audio_channels,
        //                 audio_buffers);

        // if (result == -1)
        // {
        //         ERROR_FMT("Failed to init SDL_mixer: {}", SDL_GetError());

        //         PANIC;
        // }

        // Mix_AllocateChannels(audio::g_allocated_channels);

        TRACE_FN_END;
}

static void init_renderer()
{
        TRACE_FN_BEGIN;

        if (io::g_sdl_renderer)
        {
                SDL_DestroyRenderer(io::g_sdl_renderer);
        }

        io::g_sdl_renderer = create_renderer();

        TRACE_FN_END;
}

// -----------------------------------------------------------------------------
// namespace io
// -----------------------------------------------------------------------------
namespace io
{
void init()
{
        TRACE_FN_BEGIN;

        cleanup();

        init_sdl();
        init_window();
        init_renderer();

        // TODO: Just hard coding stuff here for testing
        load_texture("dude.png");

        TRACE_FN_END;
}

void cleanup()
{
        TRACE_FN_BEGIN;

        if (g_sdl_renderer)
        {
                SDL_DestroyRenderer(g_sdl_renderer);
                g_sdl_renderer = nullptr;
        }

        if (g_sdl_window)
        {
                SDL_DestroyWindow(g_sdl_window);
                g_sdl_window = nullptr;
        }

        cleanup_sdl();

        TRACE_FN_END;
}

}  // namespace io
