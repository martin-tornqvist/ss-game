// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "state.hpp"

#include <cassert>
#include <chrono>
#include <cstdint>

#include "SDL_timer.h"
#include "debug.hpp"
#include "io.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static std::vector<std::unique_ptr<State>> s_current_states;

using Clock = std::chrono::high_resolution_clock;
using Ms = std::chrono::milliseconds;
using TimePoint = std::chrono::time_point<Clock, Ms>;

// TODO: Placeholder/guessing (se also note below)
static const auto s_ms_per_frame = Ms(16);

static TimePoint get_time_ms()
{
        return std::chrono::time_point_cast<Ms>(Clock::now());
}

static void sleep(const Ms ms)
{
        const auto start = get_time_ms();

        while (true)
        {
                const auto d = get_time_ms() - start;

                if (d >= ms)
                {
                        break;
                }
        }
}

static void run_state_iteration()
{
        const auto start = get_time_ms();

        states::start();

        if (states::is_empty())
        {
                return;
        }

        states::update();

        states::draw();

        io::update_screen();

        // TODO: Doing a really dumb and simple framerate regulation for now,
        // switch to something more sophisticated. See for example:
        // https://gameprogrammingpatterns.com/game-loop.html.
        const auto sleep_ms = start + s_ms_per_frame - get_time_ms();

        sleep(sleep_ms);
}

// -----------------------------------------------------------------------------
// namespace states
// -----------------------------------------------------------------------------
namespace states
{
void init()
{
        TRACE_FN_BEGIN;

        cleanup();

        TRACE_FN_END;
}

void cleanup()
{
        TRACE_FN_BEGIN;

        s_current_states.resize(0);

        TRACE_FN_END;
}

void run()
{
        TRACE_FN_BEGIN;

        while (!is_empty())
        {
                run_state_iteration();
        }

        TRACE_FN_END;
}

void run_until_state_done(std::unique_ptr<State> state)
{
        TRACE_FN_BEGIN;

        auto* state_addr = state.get();

        push(std::move(state));

        while (contains_state(state_addr))
        {
                run_state_iteration();
        }

        TRACE_FN_END;
}

void start()
{
        while (!is_empty() && !s_current_states.back()->has_started())
        {
                auto* const state = s_current_states.back().get();

                state->set_started();

                // NOTE: This may cause states to be pushed/popped - be careful
                // about using the "state" pointer beyond this call!
                state->on_start();
        }
}

void draw()
{
        if (is_empty())
        {
                return;
        }

        // Find the first state from the end which is NOT drawn overlayed.
        auto draw_from = std::end(s_current_states);

        while (draw_from != std::begin(s_current_states))
        {
                --draw_from;

                const auto& state_ptr = *draw_from;

                // If not drawn overlayed, draw from this state as bottom layer
                // (but only if the state has been started, see note below).
                if (!state_ptr->draw_overlayed() &&
                    state_ptr->has_started())
                {
                        break;
                }
        }

        // Draw every state from this (non-overlayed) state onward.
        for (; draw_from != std::end(s_current_states); ++draw_from)
        {
                const auto& state_ptr = *draw_from;

                // Do NOT draw states which are not yet started (they may need
                // to set up menus etc in their start function, and expect the
                // chance to do so before drawing is called).

                if (state_ptr->has_started())
                {
                        state_ptr->draw();
                }
        }
}

void update()
{
        if (is_empty())
        {
                return;
        }

        s_current_states.back()->update();
}

void push(std::unique_ptr<State> state)
{
        TRACE_FN_BEGIN;

        // Pause the current state
        if (!is_empty())
        {
                s_current_states.back()->on_paused();
        }

        s_current_states.push_back(std::move(state));

        s_current_states.back()->on_pushed();

        io::flush_input();

        TRACE_FN_END;
}

void pop()
{
        TRACE_FN_BEGIN;

        if (is_empty())
        {
                TRACE_FN_END;
                return;
        }

        s_current_states.back()->on_popped();

        s_current_states.pop_back();

        if (!is_empty())
        {
                s_current_states.back()->on_resume();
        }

        io::flush_input();

        TRACE_FN_END;
}

void pop_all()
{
        TRACE_FN_BEGIN;

        while (!is_empty())
        {
                s_current_states.back()->on_popped();

                s_current_states.pop_back();
        }

        TRACE_FN_END;
}

bool contains_state(const State* const state)
{
        return std::any_of(
                std::begin(s_current_states),
                std::end(s_current_states),
                [state](const auto& s) { return s.get() == state; });
}

bool is_current_state(const State* const state)
{
        if (is_empty())
        {
                return false;
        }

        return state == s_current_states.back().get();
}

bool is_empty()
{
        return s_current_states.empty();
}

}  // namespace states
