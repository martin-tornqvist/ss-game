// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "text_format.hpp"

#include <algorithm>

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
// Reads and removes the first word of the string.
static std::string read_and_remove_word(std::string& line)
{
        std::string str;

        for (auto it = std::begin(line); it != std::end(line);)
        {
                const char current_char = *it;

                line.erase(it);

                if (current_char == ' ')
                {
                        break;
                }

                str += current_char;
        }

        return str;
}

static bool is_word_fit(
        const std::string& current_string,
        const std::string& word_to_fit,
        const size_t max_w)
{
        return (current_string.size() + word_to_fit.size() + 1) <= max_w;
}

// -----------------------------------------------------------------------------
// namespace text_format
// -----------------------------------------------------------------------------
namespace text_format
{
std::vector<std::string> split_width(std::string str, const size_t max_w)
{
        if (str.empty())
        {
                return {};
        }

        std::string current_word = read_and_remove_word(str);

        if (str.empty())
        {
                return {current_word};
        }

        std::vector<std::string> result = {""};

        size_t current_row_idx = 0;

        while (!current_word.empty())
        {
                if (!is_word_fit(result[current_row_idx], current_word, max_w))
                {
                        // Word did not fit on current line, make a new line
                        ++current_row_idx;

                        result.emplace_back("");
                }

                // If this is not the first word on the current line, add a
                // space before the word
                if (!result[current_row_idx].empty())
                {
                        result[current_row_idx] += " ";
                }

                result[current_row_idx] += current_word;

                current_word = read_and_remove_word(str);
        }

        return result;
}

std::vector<std::string> split_delim(const std::string& str, const char delim)
{
        std::vector<std::string> result;

        std::string current_line;

        for (char c : str)
        {
                if (c == delim)
                {
                        if (!current_line.empty())
                        {
                                result.push_back(current_line);
                        }

                        current_line = "";
                }
                else
                {
                        current_line += c;
                }
        }

        if (!current_line.empty())
        {
                result.push_back(current_line);
        }

        return result;
}

std::string first_to_lower(const std::string& str)
{
        auto result = str;

        if (!result.empty())
        {
                result[0] = (char)tolower(result[0]);
        }

        return result;
}

std::string first_to_upper(const std::string& str)
{
        auto result = str;

        if (!result.empty())
        {
                result[0] = (char)toupper(result[0]);
        }

        return result;
}

std::string to_upper(const std::string& str)
{
        auto result = str;

        transform(
                std::begin(result),
                std::end(result),
                std::begin(result),
                ::toupper);

        return result;
}

}  // namespace text_format
