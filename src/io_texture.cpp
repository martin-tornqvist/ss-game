// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include <string>

#include "SDL.h"
#include "SDL_image.h"
#include "debug.hpp"
#include "io_internal.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static SDL_Surface* load_surface(const std::string& path)
{
        auto* const surface = IMG_Load(path.c_str());

        if (!surface)
        {
                TRACE_FMT(
                        "Failed to load surface from path '{}': {}",
                        path,
                        IMG_GetError());

                PANIC;
        }

        return surface;
}

static SDL_Texture* create_texture_from_surface(SDL_Surface& surface)
{
        auto* const texture =
                SDL_CreateTextureFromSurface(
                        io::g_sdl_renderer,
                        &surface);

        if (!texture)
        {
                TRACE_FMT(
                        "Failed to create texture from surface: {}",
                        IMG_GetError());

                PANIC;
        }

        return texture;
}

static void set_surface_color_key(SDL_Surface& surface /*, const Color& color*/)
{
        // TODO: Hard-coding magenta for now
        // const int v =
        //         SDL_MapRGB(
        //                 surface.format,
        //                 color.r(),
        //                 color.g(),
        //                 color.b());

        const auto v =
                SDL_MapRGB(
                        surface.format,
                        255,
                        0,
                        255);

        const bool enable_color_key = true;

        SDL_SetColorKey(&surface, enable_color_key, v);
}

// TODO: Consider something like this:
// static void verify_texture_size(
//         SDL_Texture* texture,
//         const P& expected_size,
//         const std::string& img_path)
// {
//         P size;

//         SDL_QueryTexture(texture, nullptr, nullptr, &size.x, &size.y);

//         // Verify width and height of loaded image
//         if (size != expected_size)
//         {
//                 TRACE_ERROR_RELEASE
//                         << "Tile image at \""
//                         << img_path
//                         << "\" has wrong size: "
//                         << size.x
//                         << "x"
//                         << size.x
//                         << ", expected: "
//                         << expected_size.x
//                         << "x"
//                         << expected_size.y
//                         << std::endl;

//                 PANIC;
//         }
// }

// -----------------------------------------------------------------------------
// namespace io
// -----------------------------------------------------------------------------
namespace io
{
// TODO: Placeholder
SDL_Texture* g_dude_texture = nullptr;

SDL_Texture* load_texture(const std::string& path)
{
        auto* const surface = load_surface(path);

        set_surface_color_key(*surface /*, colors::magenta() */);

        auto* const texture = create_texture_from_surface(*surface);

        SDL_FreeSurface(surface);

        // TODO: Placeholder
        g_dude_texture = texture;

        return texture;
}

void draw_texture(Pos p)
{
        p = p.scaled_up(2);

        SDL_Rect render_rect;

        render_rect.x = p.x;
        render_rect.y = p.y;

        // TODO: Hard-coded for now:
        render_rect.w = 32;
        render_rect.h = 32;

        // TODO: Placeholder
        auto* texture = g_dude_texture;

        SDL_RenderCopy(
                g_sdl_renderer,
                texture,
                nullptr,  // No clipping needed, drawing whole texture
                &render_rect);
}

}  // namespace io
