// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "game.hpp"

#include "SDL_keycode.h"
#include "SDL_scancode.h"
#include "debug.hpp"
#include "init.hpp"
#include "io.hpp"
#include "state.hpp"
#include <algorithm>
#include <cmath>
#include <iterator>
#include <memory>

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
// TODO: Placeholder
static Pos s_dude_pos(0, 0);

static game::World s_world {};

static std::shared_ptr<game::PlayerInputSys> s_player_input_sys {};
static std::shared_ptr<game::MoveSys> s_move_sys {};
static std::shared_ptr<game::DrawSys> s_draw_sys {};

// -----------------------------------------------------------------------------
// namespace world
// -----------------------------------------------------------------------------
namespace game
{
void PlayerInputSys::update()
{
        const bool is_l = io::is_key_held(SDL_SCANCODE_LEFT);
        const bool is_r = io::is_key_held(SDL_SCANCODE_RIGHT);
        const bool is_u = io::is_key_held(SDL_SCANCODE_UP);
        const bool is_d = io::is_key_held(SDL_SCANCODE_DOWN);

        // Only consider this as a move in an axis if unique direction
        // pressed in that axis.
        const bool is_hor = ((int)is_l + (int)is_r) == 1;
        const bool is_ver = ((int)is_u + (int)is_d) == 1;

        if (m_entities.size() != 1)
        {
                ASSERT(false);

                return;
        }

        auto e = *std::begin(m_entities);

        auto& vel_c = s_world.ecs.get_component<VelocityComp>(e);

        if (is_hor)
        {
                if (is_l)
                {
                        vel_c.dx = -1.0;
                }
                else if (is_r)
                {
                        vel_c.dx = 1.0;
                }
        }
        else
        {
                vel_c.dx = 0.0;
        }

        if (is_ver)
        {
                if (is_u)
                {
                        vel_c.dy = -1.0;
                }
                else if (is_d)
                {
                        vel_c.dy = 1.0;
                }
        }
        else
        {
                vel_c.dy = 0.0;
        }

        // TODO: Use """realistic""" diagonal movement?
        // if (is_hor && is_ver)
        // {
        //         vel_c.dx *= 0.7F;
        //         vel_c.dy *= 0.7F;
        // }

        vel_c.dx = std::clamp(vel_c.dx, -1.0F, 1.0F);
        vel_c.dy = std::clamp(vel_c.dy, -1.0F, 1.0F);
}

void MoveSys::update()
{
        for (const auto e : m_entities)
        {
                auto& pos_c = s_world.ecs.get_component<PosComp>(e);
                auto& vel_c = s_world.ecs.get_component<VelocityComp>(e);

                pos_c.x += vel_c.dx;
                pos_c.y += vel_c.dy;
        }
}

void DrawSys::update()
{
        for (const auto e : m_entities)
        {
                auto& pos_c = s_world.ecs.get_component<PosComp>(e);

                io::draw_texture({(int)pos_c.x, (int)pos_c.y});
        }
}

// -----------------------------------------------------------------------------
// class Gamestate
// -----------------------------------------------------------------------------
void GameState::on_start()
{
        s_world.ecs.init();

        // Register component types
        s_world.ecs.register_component<PosComp>();
        s_world.ecs.register_component<VelocityComp>();

        // Create and register systems
        s_player_input_sys = s_world.ecs.register_system<PlayerInputSys>();
        s_move_sys = s_world.ecs.register_system<MoveSys>();
        s_draw_sys = s_world.ecs.register_system<DrawSys>();

        // Register player input system signature
        // TODO: Add a component that only the player entity has?
        ecs::Signature player_system_sig;
        player_system_sig.set(s_world.ecs.get_component_type<VelocityComp>());
        s_world.ecs.set_system_signature<PlayerInputSys>(player_system_sig);

        // Register move system signature
        ecs::Signature move_system_sig;
        move_system_sig.set(s_world.ecs.get_component_type<PosComp>());
        move_system_sig.set(s_world.ecs.get_component_type<VelocityComp>());
        s_world.ecs.set_system_signature<MoveSys>(move_system_sig);

        // Register draw system signature
        ecs::Signature draw_system_sig;
        draw_system_sig.set(s_world.ecs.get_component_type<PosComp>());
        s_world.ecs.set_system_signature<DrawSys>(draw_system_sig);

        // Create player entity
        const auto player_ent = s_world.ecs.create_entity();
        s_world.ecs.add_component<PosComp>(player_ent, {0.0, 0.0});
        s_world.ecs.add_component<VelocityComp>(player_ent, {0.0, 0.0});
}

void GameState::update()
{
        auto input = io::read_input_events();

        // TODO: Placeholder
        if (input.key_released == 'q')
        {
                states::pop();

                return;
        }

        s_player_input_sys->update();
        s_move_sys->update();
}

void GameState::draw()
{
        io::clear_screen();

        s_draw_sys->update();
}

}  // namespace game
