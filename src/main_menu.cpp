// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "main_menu.hpp"

#include "game.hpp"
#include "init.hpp"
#include "io.hpp"
#include "state.hpp"

void MainMenuState::on_start()
{
}

void MainMenuState::update()
{
        auto input = io::read_input_events();

        if (input.key_released == 'q')
        {
                init::cleanup_all();

                states::pop_all();
        }
        else if (input.key_released == 'n')
        {
                auto game_state = std::make_unique<game::GameState>();
                states::push(std::move(game_state));
        }
}

void MainMenuState::draw()
{
        io::clear_screen();
}
