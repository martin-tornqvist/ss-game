// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "debug.hpp"
#include "init.hpp"
#include "io.hpp"
#include "main_menu.hpp"
#include "state.hpp"

int main(int argc, char** argv)
{
        (void)argc;
        (void)argv;

        debug::setup_logger();
        TRACE_FN_BEGIN;

        init::init_io();

        io::clear_screen();
        io::update_screen();

        auto main_menu = std::make_unique<MainMenuState>();

        states::push(std::move(main_menu));

        states::run();

        init::cleanup_io();

        TRACE_FN_END;
}
