// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "debug.hpp"

namespace debug
{
void setup_logger()
{
        auto file_logger =
                spdlog::basic_logger_mt(
                        "ss-game",
                        "game.log",
                        true);  // Overwrite log

        spdlog::set_default_logger(file_logger);
}

}  // namespace debug
