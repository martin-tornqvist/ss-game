// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "ecs.hpp"

// Credit
//
// The code in this file is mostly adapted from the following guide:
//
// Austin Morlan
// https://austinmorlan.com/posts/entity_component_system/

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// namespace ecs
// -----------------------------------------------------------------------------
namespace ecs
{
// -----------------------------------------------------------------------------
// class EntityMgr
// -----------------------------------------------------------------------------
EntityMgr::EntityMgr()
{
        for (EntityId id = 0; id < max_entities; ++id)
        {
                m_available_entity_ids.push(id);
        }
}

EntityId EntityMgr::create_entity()
{
        ASSERT(m_living_entity_count < max_entities);

        auto id = m_available_entity_ids.front();

        m_available_entity_ids.pop();

        ++m_living_entity_count;

        return id;
}

void EntityMgr::destroy_entity(const EntityId entity)
{
        ASSERT(entity < max_entities);

        m_signatures[entity].reset();

        m_available_entity_ids.push(entity);

        --m_living_entity_count;
}

void EntityMgr::set_signature(
        const EntityId entity,
        const Signature signature)
{
        ASSERT(entity < max_entities);

        m_signatures[entity] = signature;
}

Signature EntityMgr::get_signature(const EntityId entity)
{
        ASSERT(entity < max_entities);

        return m_signatures[entity];
}

}  // namespace ecs
