// =============================================================================
// Copyright 2011-2020 Martin Törnqvist <m.tornq@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "init.hpp"

#include "io.hpp"

namespace init
{
void cleanup_all()
{
        cleanup_session();
        cleanup_io();
}

void init_io()
{
        io::init();
}

void cleanup_io()
{
        io::cleanup();
}

void init_session()
{
}

void cleanup_session()
{
}

}  // namespace init
