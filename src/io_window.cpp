// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include "io.hpp"

#include <cstdint>

#include "SDL.h"
#include "SDL_video.h"
#include "debug.hpp"
#include "io_internal.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static Pos get_sdl_native_resolution()
{
        SDL_DisplayMode display_mode;

        const auto result = SDL_GetDesktopDisplayMode(0, &display_mode);

        if (result != 0)
        {
                ERROR_FMT("Failed to read native resolution", SDL_GetError());

                PANIC;
        }

        return {display_mode.w, display_mode.h};
}

static std::string make_window_title()
{
        // TODO: Add version / git revision
        std::string title = "COOL GAME v0.00001";

        // if (version_info::g_version_str.empty())
        // {
        //         const auto git_sha1_str =
        //                 version_info::read_git_sha1_str_from_file();

        //         title +=
        //                 "(build " +
        //                 git_sha1_str +
        //                 ", " +
        //                 version_info::g_date_str +
        //                 ")";
        // }
        // else
        // {
        //         // Version string defined
        //         title += version_info::g_version_str;
        // }

        return title;
}

static SDL_Window* create_sdl_window(const Pos& px_dims)
{
        TRACE_FN_BEGIN;

        TRACE_FMT(
                "Attempting to create window with size: {}x{}",
                px_dims.x,
                px_dims.y);

        SDL_Window* window = nullptr;

        const auto title = make_window_title();

        window =
                SDL_CreateWindow(
                        title.c_str(),
                        SDL_WINDOWPOS_CENTERED,
                        SDL_WINDOWPOS_CENTERED,
                        px_dims.x,
                        px_dims.y,
                        SDL_WINDOW_SHOWN);

        if (!window)
        {
                ERROR_FMT("Failed to create window: {}", SDL_GetError());
        }

        TRACE_FN_END;

        return window;
}

static SDL_Window* init_window_windowed()
{
        const Pos size = Pos(800, 600);

        const auto logical_size = size.scaled_down(2);

        TRACE_FMT("Window size: {}x{}", size.x, size.y);
        TRACE_FMT("Window logical size: {}x{}", logical_size.x, logical_size.x);

        auto* const window = create_sdl_window(size);

        return window;
}

// -----------------------------------------------------------------------------
// namespace io
// -----------------------------------------------------------------------------
namespace io
{
SDL_Window* g_sdl_window = nullptr;
SDL_Renderer* g_sdl_renderer = nullptr;

void init_window()
{
        if (g_sdl_window)
        {
                SDL_DestroyWindow(g_sdl_window);

                g_sdl_window = nullptr;
        }

        const auto native_res = get_sdl_native_resolution();

        TRACE_FMT("Native resolution: {}x{}", native_res.x, native_res.y);

        g_sdl_window = init_window_windowed();

        if (!g_sdl_window)
        {
                ERROR_FMT("Failed to set up window: {}", SDL_GetError());

                PANIC;
        }
}

void update_screen()
{
        // const auto screen_panel_dims = panel_px_dims(Panel::screen);

        // if (!panels::is_valid() &&
        //     (screen_panel_dims.x > config::gui_cell_px_w()) &&
        //     (screen_panel_dims.y > config::gui_cell_px_h()))
        // {
        //         draw_text_at_px(
        //                 "Window too small",
        //                 {0, 0},
        //                 colors::light_white(),
        //                 DrawBg::no,
        //                 colors::black());
        // }

        SDL_RenderPresent(g_sdl_renderer);
}

void clear_screen()
{
        SDL_SetRenderDrawColor(g_sdl_renderer, 0U, 0U, 0U, 0xFFU);

        SDL_RenderClear(g_sdl_renderer);
}

}  // namespace io
