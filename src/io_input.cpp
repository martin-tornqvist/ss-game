// =============================================================================
// TODO : What should the copyright be ?
// Copyright 2021 ...
//
// SPDX-License-Identifier: AGPL-3.0-or-later
// =============================================================================

#include <cstdint>
#include <ostream>
#include <pthread.h>

#include "SDL_events.h"
#include "SDL_keyboard.h"
#include "SDL_keycode.h"
#include "SDL_timer.h"
#include "debug.hpp"
#include "init.hpp"
#include "io.hpp"
#include "pos.hpp"
#include "state.hpp"

// -----------------------------------------------------------------------------
// Private
// -----------------------------------------------------------------------------
static SDL_Event s_sdl_event;

static InputData s_input;

static void handle_window_event()
{
        switch (s_sdl_event.window.event)
        {
        case SDL_WINDOWEVENT_RESTORED:
        {
                TRACE("Window restored");
        }
        // Fallthrough
        case SDL_WINDOWEVENT_FOCUS_GAINED:
        {
                TRACE("Window gained focus");

                io::clear_events();
        }
        // Fallthrough
        case SDL_WINDOWEVENT_EXPOSED:
        {
                TRACE("Window exposed");
        }
        break;

        default:
        {
        }
        break;
        }
}

static void handle_quit_event()
{
        // TODO: For now we just brutally exit the game.
        init::cleanup_all();

        states::pop_all();
}

// -----------------------------------------------------------------------------
// io
// -----------------------------------------------------------------------------
namespace io
{
void flush_input()
{
        SDL_PumpEvents();
}

void clear_events()
{
        while (SDL_PollEvent(&s_sdl_event))
        {
        }
}

bool is_key_held(int key)
{
        const uint8_t* key_states = SDL_GetKeyboardState(nullptr);

        const bool is_held = key_states[key];

        return is_held;
}

InputData read_input_events()
{
        s_input.key_pressed = -1;
        s_input.key_released = -1;

        const bool did_poll_event = SDL_PollEvent(&s_sdl_event);

        if (!did_poll_event)
        {
                return s_input;
        }

        switch (s_sdl_event.type)
        {
        case SDL_WINDOWEVENT:
        {
                handle_window_event();
        }
        break;

        case SDL_QUIT:
        {
                handle_quit_event();
        }
        break;

        case SDL_KEYDOWN:
        {
                const auto sdl_keysym = s_sdl_event.key.keysym.sym;

                s_input.key_pressed = sdl_keysym;
        }
        break;

        case SDL_KEYUP:
        {
                const auto sdl_keysym = s_sdl_event.key.keysym.sym;

                s_input.key_released = sdl_keysym;
        }
        break;

        case SDL_TEXTINPUT:
        {
        }
        break;

        default:
        {
        }
        break;
        }

        return s_input;
}

}  // namespace io
