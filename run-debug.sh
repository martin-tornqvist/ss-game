#!/usr/bin/env sh

set -xue

root_dir=$PWD

./build-debug.sh

cd build

./ss-game-debug

cd $root_dir
